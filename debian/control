Source: golang-github-canonical-candid
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Mathias Gibbens <gibmat@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-coreos-go-oidc-dev,
               golang-github-frankban-quicktest-dev,
               golang-github-go-ldap-ldap-dev (>= 3.4.5),
               golang-github-go-macaroon-bakery-macaroon-bakery-dev,
               golang-github-go-webauthn-webauthn-dev,
               golang-github-gomodule-oauth1-dev,
               golang-github-google-go-cmp-dev,
               golang-github-google-uuid-dev,
               golang-github-gorilla-handlers-dev,
               golang-github-juju-aclstore-dev,
               golang-github-juju-clock-dev,
               golang-github-juju-cmd-dev (>= 3.0.2),
               golang-github-juju-gnuflag-dev,
               golang-github-juju-loggo-dev,
               golang-github-juju-names-dev,
               golang-github-juju-persistent-cookiejar-dev,
               golang-github-juju-qthttptest-dev,
               golang-github-juju-schema-dev,
               golang-github-juju-simplekv-dev,
               golang-github-juju-testing-dev,
               golang-github-juju-usso-dev,
               golang-github-juju-utils-dev,
               golang-github-julienschmidt-httprouter-dev,
               golang-github-mhilton-openid-dev,
               golang-github-yohcop-openid-go-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-net-dev,
               golang-golang-x-oauth2-dev,
               golang-gopkg-asn1-ber.v1-dev,
               golang-gopkg-errgo.v1-dev,
               golang-gopkg-goose.v1-dev,
               golang-gopkg-httprequest.v1-dev,
               golang-gopkg-juju-environschema.v1-dev,
               golang-gopkg-macaroon.v2-dev,
               golang-gopkg-mgo.v2-dev,
               golang-gopkg-natefinch-lumberjack.v2-dev,
               golang-gopkg-square-go-jose.v2-dev,
               golang-gopkg-tomb.v2-dev,
               golang-pq-dev,
               golang-prometheus-client-dev,
               golang-yaml.v2-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-canonical-candid
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-canonical-candid.git
Homepage: https://github.com/canonical/candid
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/canonical/candid

Package: golang-github-canonical-candid-dev
Architecture: all
Multi-Arch: foreign
Breaks: lxd (<< 5.0.1)
Depends: ${misc:Depends},
         golang-github-coreos-go-oidc-dev,
         golang-github-frankban-quicktest-dev,
         golang-github-go-ldap-ldap-dev (>= 3.4.5),
         golang-github-go-macaroon-bakery-macaroon-bakery-dev,
         golang-github-go-webauthn-webauthn-dev,
         golang-github-gomodule-oauth1-dev,
         golang-github-google-go-cmp-dev,
         golang-github-google-uuid-dev,
         golang-github-gorilla-handlers-dev,
         golang-github-juju-aclstore-dev,
         golang-github-juju-clock-dev,
         golang-github-juju-cmd-dev (>= 3.0.2),
         golang-github-juju-gnuflag-dev,
         golang-github-juju-loggo-dev,
         golang-github-juju-names-dev,
         golang-github-juju-persistent-cookiejar-dev,
         golang-github-juju-qthttptest-dev,
         golang-github-juju-schema-dev,
         golang-github-juju-simplekv-dev,
         golang-github-juju-testing-dev,
         golang-github-juju-usso-dev,
         golang-github-juju-utils-dev,
         golang-github-julienschmidt-httprouter-dev,
         golang-github-mhilton-openid-dev,
         golang-github-yohcop-openid-go-dev,
         golang-golang-x-crypto-dev,
         golang-golang-x-net-dev,
         golang-golang-x-oauth2-dev,
         golang-gopkg-asn1-ber.v1-dev,
         golang-gopkg-errgo.v1-dev,
         golang-gopkg-goose.v1-dev,
         golang-gopkg-httprequest.v1-dev,
         golang-gopkg-juju-environschema.v1-dev,
         golang-gopkg-macaroon.v2-dev,
         golang-gopkg-mgo.v2-dev,
         golang-gopkg-natefinch-lumberjack.v2-dev,
         golang-gopkg-square-go-jose.v2-dev,
         golang-gopkg-tomb.v2-dev,
         golang-pq-dev,
         golang-prometheus-client-dev,
         golang-yaml.v2-dev
Description: Candid Identity Manager Service (library)
 The Candid server provides a macaroon-based authentication service.
 .
 The Go source code for this library is packaged in Debian with the
 sole purpose of building other Go programs for Debian.
 .
 Regular users should not depend on this package in their regular
 development workflow. For that, users should use `go get`.
